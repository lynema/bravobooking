package org.lynema.bravobooking.type;

public enum OccupationStatus {
	VACANT, OCCUPIED
}

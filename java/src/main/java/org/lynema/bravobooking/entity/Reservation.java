package org.lynema.bravobooking.entity;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Builder
@Getter
@ToString
public class Reservation {
	@NonNull
	private Integer numberOfGuests;
	@NonNull
	private Integer numberOfPets;
	@NonNull
	private LocalDate firstDayOfReservation;
	@NonNull
	private LocalDate lastDayOfReservation;
	private boolean handicapAccessible;
}

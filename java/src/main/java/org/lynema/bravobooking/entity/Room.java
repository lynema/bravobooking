package org.lynema.bravobooking.entity;

import java.time.LocalDate;
import java.util.Objects;

import org.lynema.bravobooking.type.OccupationStatus;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Room {
	@EqualsAndHashCode.Include
	private Integer floor;
	@EqualsAndHashCode.Include
	private Integer number;
	private OccupationStatus occupationStatus;
	private LocalDate nextAvailableDate;
	public boolean isPetAllowed() {
		return (Objects.nonNull(floor) && floor.equals(0));
	}
	public boolean isAccessible() {
		return (Objects.nonNull(floor) && floor.equals(0));
	}
}

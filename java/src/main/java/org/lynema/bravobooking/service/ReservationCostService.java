package org.lynema.bravobooking.service;

import java.util.function.Function;

import org.lynema.bravobooking.entity.Reservation;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ReservationCostService {
	private static int ONE_BED_COST = 50;
	private static int TWO_BED_COST = 75;
	private static int THREE_BED_COST = 90;
	private static int PER_PET_COST = 20;
	private static int INVALID_COST = 1000000000;

	public Function<Reservation, Integer> getReservationCost = (reservation) -> {
		int cost = 0;
		switch (reservation.getNumberOfGuests()) {
		case 1:
			cost += ONE_BED_COST;
			break;
		case 2:
			cost += TWO_BED_COST;
			break;
		case 3:
			cost += THREE_BED_COST;
			break;
		default:
			cost += INVALID_COST;
		}
		switch (reservation.getNumberOfPets()) {
		case 0:
			break;
		case 1:
		case 2:
			cost += PER_PET_COST * reservation.getNumberOfPets();
			break;
		default:
			cost += INVALID_COST;
		}
		return (cost);
	};
}

package org.lynema.bravobooking.service;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.lynema.bravobooking.dao.MotelDAO;
import org.lynema.bravobooking.entity.Reservation;
import org.lynema.bravobooking.entity.Room;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BookingService {
	private final MotelDAO motelDAO;

	public Map<Integer, Set<Room>> findAvailableScoredRoomMap(Reservation reservation) {

		// filter out booked rooms
		Set<Room> availableRooms = motelDAO.getAvailableRoomSetForReservation(reservation);

		// get a score for each room and return a map of scores tied to rooms
		ConcurrentSkipListMap<Integer, Set<Room>> scoredRoomMap = availableRooms.parallelStream()
				.collect(Collectors.groupingByConcurrent((room) -> scoreRoom.apply(room, reservation),
						ConcurrentSkipListMap::new, Collectors.toCollection(LinkedHashSet::new)));
		//The order is reversed by default so it is switched back.  The best matched rooms are first.
		return scoredRoomMap.descendingMap();
	}

	public boolean bookRoom(Room room, Reservation reservation) {
		return motelDAO.bookRoom(room, reservation);
	}

	private BiFunction<Room, Reservation, Integer> scoreRoom = (room, reservation) -> {
		Integer score = 0;
		score += ReservationScoreService.scoreNumberOfGuests.apply(reservation);
		score += ReservationScoreService.scoreHandicapAccessible.apply(reservation, room);
		score += ReservationScoreService.scoreNumberOfPets.apply(reservation, room);
		return score;
	};
}

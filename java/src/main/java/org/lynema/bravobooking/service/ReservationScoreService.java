package org.lynema.bravobooking.service;

import java.util.function.BiFunction;
import java.util.function.Function;

import org.lynema.bravobooking.entity.Reservation;
import org.lynema.bravobooking.entity.Room;

import lombok.experimental.UtilityClass;

@UtilityClass
class ReservationScoreService {
	public static int OK = 0;
	private static int FAIL = -1000;
	private static int PREFER_LESS_ACCESSIBLE = 5;
	private static int MUST_HAVE = 500;

	private static int MAX_NUMBER_OF_GUESTS_PER_ROOM = 3;
	private static int MAX_NUMBER_OF_PETS_PER_ROOM = 2;
	
	
	Function<Reservation, Integer> scoreNumberOfGuests = (reservation) -> {
		return (reservation.getNumberOfGuests() > MAX_NUMBER_OF_GUESTS_PER_ROOM ? FAIL : OK);
	};

	
	BiFunction<Reservation, Room, Integer> scoreNumberOfPets = (reservation, room) -> {
		if (reservation.getNumberOfPets() > MAX_NUMBER_OF_PETS_PER_ROOM) {
			return FAIL;
		}
		if (reservation.getNumberOfPets() > 0)
		{
			return (room.isPetAllowed() ? MUST_HAVE : FAIL);
		}
		else
		{
			return (room.isPetAllowed() ? OK : PREFER_LESS_ACCESSIBLE);
		}
	};

	
	BiFunction<Reservation, Room, Integer> scoreHandicapAccessible = (reservation, room) -> {
		if (reservation.isHandicapAccessible()) {
			return (room.isAccessible() ? MUST_HAVE : FAIL);
		}
		else
		{
			return (room.isAccessible() ? OK : PREFER_LESS_ACCESSIBLE);
		}
	};
}

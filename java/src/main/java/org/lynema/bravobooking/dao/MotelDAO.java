package org.lynema.bravobooking.dao;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.lynema.bravobooking.entity.Reservation;
import org.lynema.bravobooking.entity.Room;

public class MotelDAO {
	public MotelDAO(Integer roomsPerFloor, Integer numberOfFloors) {
		roomSet = new ConcurrentHashMap<Room,Set<Reservation>>();
		for (int floor = 0; floor < numberOfFloors; floor++)
			for (int room = 0; room < roomsPerFloor; room++) {
				roomSet.putIfAbsent(Room.builder().floor(floor).number(room).build(), 
						Collections.synchronizedSet(new LinkedHashSet<Reservation>()));
			}
	}

	private ConcurrentHashMap<Room,Set<Reservation>> roomSet;
	
	private BiPredicate<Reservation, Reservation> isReservationOverlapping = (existingReservation, newReservation) -> {
		if (existingReservation.getLastDayOfReservation().isBefore(newReservation.getFirstDayOfReservation())
				|| newReservation.getLastDayOfReservation().isBefore(existingReservation.getFirstDayOfReservation())) {
			return false;
		} else {
			return true;
		}
	};
	
	public synchronized boolean bookRoom(Room room, Reservation reservation) {
		var currentReservations = roomSet.get(room);
		if (!isRoomAvailable.test(currentReservations, reservation))
		{
			return false;
		}
		else
		{
			currentReservations.add(reservation);
			return true;
		}
	}

	private BiPredicate<Set<Reservation>, Reservation> isRoomAvailable = (reservationSet, newReservation) -> {
		return reservationSet.parallelStream().noneMatch(
				(existingReservation) -> isReservationOverlapping.test(existingReservation, newReservation));
	};

	public Set<Room> getAvailableRoomSetForReservation(Reservation reservation) {
		return this.roomSet.entrySet().parallelStream()
		.filter((currentReservations) -> isRoomAvailable.test(currentReservations.getValue(), reservation))
		.map(Entry::getKey).collect(Collectors.toCollection(LinkedHashSet::new));
	}
}

package org.lynema.bravobooking;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Iterator;

import org.junit.Test;
import org.lynema.bravobooking.dao.MotelDAO;
import org.lynema.bravobooking.entity.Reservation;
import org.lynema.bravobooking.entity.Room;
import org.lynema.bravobooking.service.BookingService;
import org.lynema.bravobooking.service.ReservationCostService;

public class BravoBookingTest {
	private static Integer ROOM_COUNT = 10;

	@Test
	public void testInvalidGuests() {
		MotelDAO motelDAO = new MotelDAO(ROOM_COUNT, 2);
		BookingService bookingService = new BookingService(motelDAO);
		Reservation reservation = Reservation.builder().numberOfGuests(4).numberOfPets(2).handicapAccessible(false)
				.firstDayOfReservation(LocalDate.now()).lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		var roomsByScore = bookingService.findAvailableScoredRoomMap(reservation);
		assertFalse(roomsByScore.keySet().parallelStream().anyMatch((i) -> i > 0));
	}

	@Test
	public void testInvalidPets() {
		MotelDAO motelDAO = new MotelDAO(ROOM_COUNT, 2);
		BookingService bookingService = new BookingService(motelDAO);
		Reservation reservation = Reservation.builder().numberOfGuests(1).numberOfPets(3).handicapAccessible(false)
				.firstDayOfReservation(LocalDate.now()).lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		var roomsByScore = bookingService.findAvailableScoredRoomMap(reservation);
		assertFalse(roomsByScore.keySet().parallelStream().anyMatch((i) -> i > 0));
	}

	@Test
	public void testPets() {
		MotelDAO motelDAO = new MotelDAO(ROOM_COUNT, 2);
		BookingService bookingService = new BookingService(motelDAO);
		Reservation reservation = Reservation.builder().numberOfGuests(2).numberOfPets(2).handicapAccessible(false)
				.firstDayOfReservation(LocalDate.now()).lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		var roomsByScore = bookingService.findAvailableScoredRoomMap(reservation);
		assertTrue(roomsByScore.keySet().contains((-995)));
		assertTrue(roomsByScore.keySet().contains((500)));
		assertTrue(roomsByScore.get(-995).size() == ROOM_COUNT);
		assertTrue(roomsByScore.get(500).size() == ROOM_COUNT);
	}

	@Test
	public void testHandicapped() {
		MotelDAO motelDAO = new MotelDAO(ROOM_COUNT, 2);
		BookingService bookingService = new BookingService(motelDAO);
		Reservation reservation = Reservation.builder().numberOfGuests(2).numberOfPets(2).handicapAccessible(true)
				.firstDayOfReservation(LocalDate.now()).lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		var roomsByScore = bookingService.findAvailableScoredRoomMap(reservation);
		assertTrue(roomsByScore.keySet().contains((-2000)));
		assertTrue(roomsByScore.keySet().contains((1000)));
		assertTrue(roomsByScore.get(-2000).size() == ROOM_COUNT);
		assertTrue(roomsByScore.get(1000).size() == ROOM_COUNT);
	}

	@Test
	public void testReservationCosts() {
		assertTrue(ReservationCostService.getReservationCost.apply((Reservation.builder().numberOfGuests(2)
				.numberOfPets(2).handicapAccessible(true).firstDayOfReservation(LocalDate.now())
				.lastDayOfReservation(LocalDate.now().plusDays(3)).build())) == 115);
		assertTrue(ReservationCostService.getReservationCost.apply((Reservation.builder().numberOfGuests(1)
				.numberOfPets(0).handicapAccessible(true).firstDayOfReservation(LocalDate.now())
				.lastDayOfReservation(LocalDate.now().plusDays(3)).build())) == 50);
		assertTrue(ReservationCostService.getReservationCost.apply((Reservation.builder().numberOfGuests(3)
				.numberOfPets(1).handicapAccessible(true).firstDayOfReservation(LocalDate.now())
				.lastDayOfReservation(LocalDate.now().plusDays(3)).build())) == 110);
	}

	@Test
	public void testDoubleBookingRoom() {
		MotelDAO motelDAO = new MotelDAO(ROOM_COUNT, 2);
		BookingService bookingService = new BookingService(motelDAO);
		Reservation reservation = Reservation.builder().numberOfGuests(2).numberOfPets(2).handicapAccessible(true)
				.firstDayOfReservation(LocalDate.now()).lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		var roomsByScore = bookingService.findAvailableScoredRoomMap(reservation);
		assertTrue(roomsByScore.entrySet().stream().findFirst().get().getValue().size() == ROOM_COUNT);
		var roomToBook = roomsByScore.entrySet().stream().findFirst().get().getValue().iterator().next();
		// reserve one of the ROOM_COUNT Available ROOMS
		assertTrue(bookingService.bookRoom(roomToBook, reservation));
		assertFalse(bookingService.bookRoom(roomToBook, reservation));
		roomsByScore = bookingService.findAvailableScoredRoomMap(reservation);
		assertTrue(roomsByScore.entrySet().stream().findFirst().get().getValue().size() == ROOM_COUNT - 1);
	}

	@Test
	public void preferredRoomsBookedFirst() {
		MotelDAO motelDAO = new MotelDAO(ROOM_COUNT, 2);
		BookingService bookingService = new BookingService(motelDAO);
		Reservation offFirstFloorReservation = Reservation.builder().numberOfGuests(2).numberOfPets(0)
				.handicapAccessible(false).firstDayOfReservation(LocalDate.now())
				.lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		var roomsByScore = bookingService.findAvailableScoredRoomMap(offFirstFloorReservation);
		assertTrue(roomsByScore.entrySet().stream().findFirst().get().getValue().size() == ROOM_COUNT);
		Iterator<Room> bestRoomsToBook = roomsByScore.entrySet().stream().findFirst().get().getValue().iterator();
		bestRoomsToBook.forEachRemaining((room) -> {
			assertTrue(bookingService.bookRoom(room, offFirstFloorReservation));
		});

		var accessibleReservation = Reservation.builder().numberOfGuests(2).numberOfPets(2).handicapAccessible(true)
				.firstDayOfReservation(LocalDate.now()).lastDayOfReservation(LocalDate.now().plusDays(3)).build();

		roomsByScore = bookingService.findAvailableScoredRoomMap(accessibleReservation);
		assertTrue(roomsByScore.keySet().parallelStream().anyMatch((i) -> i > 0));
		assertTrue(roomsByScore.entrySet().stream().findFirst().get().getValue().size() == ROOM_COUNT);
	}
}
